---
title: "Curriculum Vitae et plus"
date: 2019-07-17T23:09:28+02:00
draft: false
---

## [Télécharger le CV](https://savaneprod.fr/CV_JFD.pdf)
#### [870 sur 990 au TOEIC le 26 Février 2019](https://savaneprod.fr/TOEIC.png)
#### [Certification ITIL Foundation, obtenue en Avril 2019](https://savaneprod.fr/Cert_ITIL.pdf)
#### [Dépôt framagit](https://framagit.org/pileus)
